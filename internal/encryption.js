const crypto = require('crypto');

module.exports = {
  getKeyFromPassword: (password, salt, length, iterationCount = 100000) => crypto.pbkdf2Sync(password, salt, iterationCount, length, 'sha256'),
  encrypt: (text, key, algorithm, callback) => {
    crypto.randomBytes(16, (err, iv) => {
      if (err) callback(err);
      try {
        const encipher = crypto.createCipheriv(algorithm, key, iv);
        const encBuffer = Buffer.concat([iv, encipher.update(text, 'utf8'), encipher.final()]);
        callback(null, encBuffer);
      } catch (err2) {
        callback(err2);
      }
    });
  },
  decrypt: (enc, key, algorithm, callback) => {
    try {
      const buffer = Buffer.from(enc, 'base64');
      const iv = buffer.slice(0, 16);
      const enc2 = buffer.slice(16);
      const decipher = crypto.createDecipheriv(algorithm, key, iv);
      const decBuffer = Buffer.concat([decipher.update(enc2), decipher.final()]);
      callback(null, decBuffer.toString('utf8'));
    } catch (err) {
      callback(err);
    }
  },
};
