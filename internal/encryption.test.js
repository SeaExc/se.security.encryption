const chai = require('chai');

const { expect } = chai;

const encryption = require('./encryption');

const algorithm = 'aes-256-cbc';

describe('Internal tests', () => {
  it('Can find internal module', () => {
    expect(encryption).to.exist;
  });

  it('Can get encryptionKey from password and Salt', () => {
    expect(encryption.getKeyFromPassword('My Password', 'My Salt', 32).toString('base64')).to.equal('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=');
  });

  it('Can encrypt string', (done) => {
    const key = Buffer.from('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', 'base64');
    encryption.encrypt('This is somethig to encrypt', key, algorithm, (err, enc) => {
      expect(err).to.be.null;
      expect(enc).to.not.be.undefined;
      done();
    });
  });

  it('Can decrypt string', (done) => {
    const key = Buffer.from('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', 'base64');
    const text = 'My text to encrypt';

    encryption.encrypt(text, key, algorithm, (err, enc) => {
      encryption.decrypt(enc, key, algorithm, (err2, dec) => {
        expect(dec).to.equal(text);
        done();
      });
    });
  });

  it('Will return error if decryption string is broken', (done) => {
    const key = Buffer.from('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', 'base64');
    encryption.decrypt('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', key, algorithm, (err) => {
      expect(err).to.not.be.null;
      done();
    });
  });

  it('Will return error if decryption algorithm is invalid', (done) => {
    const key = Buffer.from('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', 'base64');
    encryption.decrypt('rp/UHydy9exXO7R7NWYA7cqMbzt4MBAO5kdZO18Y4wI=', key, 'invalid-alg', (err) => {
      expect(err).to.not.be.null;
      done();
    });
  });
});
