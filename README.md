# Encryption SE

[![NPM Version](https://img.shields.io/npm/v/encryption-se.svg?style=flat)](https://www.npmjs.org/package/encryption-se)
[![NPM Downloads](https://img.shields.io/npm/dm/encryption-se.svg?style=flat)](https://npmcharts.com/compare/encryption-se?minimal=true)
[![Install Size](https://packagephobia.now.sh/badge?p=encryption-se)](https://packagephobia.now.sh/result?p=encryption-se)
[![CircleCI](https://circleci.com/bb/SeaExc/se.security.encryption.svg?style=shield)](https://circleci.com/bb/SeaExc/se.security.encryption)
[![Coverage Status](https://coveralls.io/repos/bitbucket/SeaExc/se.security.encryption/badge.svg?branch=master)](https://coveralls.io/bitbucket/SeaExc/se.security.encryption?branch=master)

### Description
A simple module wrapping around NodeJS Crypto module allowing handling IV by prefixing it to encrypted string.

## Options
Name | Default | Description
:--- | --- | :---
`password` | `null` | Password secret **REQUIRED**
`passwordSalt` | `This is my salt 362` | Password salt
`iterationCount` | `100000` | Number of iterations for PBKDF2
`encryptionKey` | `null` | Encryption key as array of bytes

### How to use it
#### Installation

```shell

// First you have to get module and instantiate it

npm install encryption-se

OR

yarn add encryption-se

```

After installing module, add this to your code:

```javascript
const options = {
  password: process.env.encryptionPassword || 'SomePassword'
};

const encryption = require('encryption-se')(options);
```

#### How to encrypt?
```javascript
encryption
  .encrypt('This is to be encrypted')
  .then(enc => {
    // 'enc' contains encrypted string in base64 format
  })
  .catch((err) => {
    // This is to handle errors
  })
```

####  How to decrypt?
```javascript
encryption
  .decrypt('iQ6qlRWlwWXtmGPFbBiEc4WKKAbHCLQK0+HLxoGLKY0=')
  .then((text) => {
    // 'text' contains decrypted string
  })
  .catch((err) => {
    // This is to handle errors
  })
```

### Contact

[![Image](https://static-exp1.licdn.com/scds/common/u/images/logos/linkedin/logo_linkedin_93x21_v2.png)](https://www.linkedin.com/in/mihovilstrujic)