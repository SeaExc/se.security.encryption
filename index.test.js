const chai = require('chai');
const sinon = require('sinon');

const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

const { expect } = chai;

chai.should();

const encryptionModule = require('./index');

const encryption = encryptionModule({ password: 'SomePasswordForTesting', passwordSalt: 'PeterPan123' });

describe('Encryption Tests', () => {
  describe('Constructor', () => {
    it('Rejects if there are no options specified', () => {
      expect(encryptionModule.bind(encryptionModule)).to.throw('You have to specify options for encryption module');
    });

    it('Is able to receive iterationsCount as number', () => {
      const enc = encryptionModule({
        password: 'My password',
        passwordSalt: 'SaltIt',
        iterationCount: 234213,
      });

      expect(enc).to.not.throw;
      expect(enc.options.iterationCount).to.equal(234213);
    });

    it('Is able to receive iterationsCount as string', () => {
      const enc = encryptionModule({
        password: 'My password',
        passwordSalt: 'SaltIt',
        iterationCount: '234213',
      });

      expect(enc).to.not.throw;
      expect(enc.options.iterationCount).to.equal(234213);
    });

    it('Is able to initialize without iterationsCount specified', () => {
      const enc = encryptionModule({
        password: 'My password',
        passwordSalt: 'SaltIt',
      });

      expect(enc).to.not.throw;
      expect(enc.options.iterationCount).to.equal(100000);
    });

    it('Rejects if there is no password nor encryptionKey', () => {
      expect(encryptionModule.bind(encryptionModule, {})).to.throw('You have to specify password or encryption key');
    });

    it('Sets a default PasswordSalt if there is only password provided', () => {
      const spy = sinon.spy(console, 'log');
      expect(encryptionModule({ password: 'My password' }).options.passwordSalt).to.equal('PeterPan123');
      expect(spy.calledWith('Warning: Default salt string used, you should specify passwordSalt option')).to.be.true;
    });

    it('Creates key if there is password provided', () => {
      expect(encryptionModule({
        password: 'My password',
        passwordSalt: 'My salt',
      }).options.encryptionKey).not.to.be.undefined;
    });

    it('Creates same key for same password and salt provided', () => {
      const options = { password: 'My password', passwordSalt: 'My salt' };
      expect(encryptionModule(options).options.encryptionKey).to.equal(encryptionModule(options).options.encryptionKey);
    });

    it('Creates different key if password is same but salt is different', () => {
      const options1 = { password: 'My password', passwordSalt: 'My salt' };
      const options2 = { password: 'My password', passwordSalt: 'My salt different' };
      expect(encryptionModule(options1).options.encryptionKey).to.not.equal(encryptionModule(options2).options.encryptionKey);
    });

    it('Creates different key if password is different and salt is same', () => {
      const options1 = { password: 'My password', passwordSalt: 'My salt' };
      const options2 = { password: 'My password different', passwordSalt: 'My salt' };
      expect(encryptionModule(options1).options.encryptionKey).to.not.equal(encryptionModule(options2).options.encryptionKey);
    });
  });

  it('Can get key from password', (done) => {
    expect(encryption.options.encryptionKey.toString('base64')).to.equal('iQ6qlRWlwWXtmGPFbBiEc4WKKAbHCLQK0+HLxoGLKY0=');
    done();
  });

  it('Can encrypt string', (done) => {
    encryption.encrypt('BlaBla')
      .then((enc) => {
        expect(enc).to.exist;
        done();
      });
  });

  it('Will encrypt same string to different blob every time repeated', (done) => {
    let enc1;
    let enc2;
    Promise.all([
      encryption.encrypt('SomeText').then((res) => {
        enc1 = res;
      }),
      encryption.encrypt('SomeText').then((res) => {
        enc2 = res;
      }),
    ]).then(() => {
      expect(enc1).not.to.be.equal(enc2);
      done();
    });
  });

  it('Can decrypt string', (done) => {
    const text = 'ExampleText';
    encryption
      .encrypt(text)
      .then(encryption.decrypt)
      .then((decText) => {
        expect(decText).to.be.equal(text);
      })
      .catch(() => {
        console.log('Rejected!');
      })
      .should.not.be.rejected
      .then(() => {
        done();
      });
  });
});
