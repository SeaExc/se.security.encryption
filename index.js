const internal = require('./internal/encryption');

module.exports = (options) => {
  // Check options
  const opt = options;
  if (!opt) throw Error('You have to specify options for encryption module');

  if (!opt.iterationCount) opt.iterationCount = 100000;
  else if (opt.iterationCount && typeof opt.iterationCount === 'string') opt.iterationCount = parseInt(opt.iterationCount, 10);

  if (!opt.password && !opt.encryptionKey) throw Error('You have to specify password or encryption key');

  if (opt.password && !opt.encryptionKey) {
    if (!opt.passwordSalt) {
      console.log('Warning: Default salt string used, you should specify passwordSalt option');
      opt.passwordSalt = 'PeterPan123';
    }
    opt.encryptionKey = internal.getKeyFromPassword(opt.password, opt.passwordSalt, 32, opt.iterationCount);
  }

  if (!opt.encryptionAlgorithm) opt.encryptionAlgorithm = 'aes-256-cbc';

  return {
    options,
    encrypt: (text) => new Promise((resolve) => {
      internal.encrypt(text, options.encryptionKey, options.encryptionAlgorithm, (err, result) => {
        if (err) throw err;
        resolve(result.toString('base64'));
      });
    }),
    decrypt: (encText) => new Promise((resolve) => {
      internal.decrypt(encText,
        options.encryptionKey,
        options.encryptionAlgorithm, (err, result) => {
          if (err) throw err;
          resolve(result);
        });
    }),
  };
};
